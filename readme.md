Functions, shell (lite).

Pre-alpha, see `b25c535b582f8ecb2632e48b2c940565e05cc3d1` for roadmap.

Mostly about automating a robust, minimal, sustainably sourced build pipeline.

# usage 

command example(s):  
`path/to/f.sh --option value "path or target"`  
eg  
`f --install depends ./nginx.depends`  
or  
`f -v`


# quickstart

`bash <(curl -s https://functions.sh/)`

This executes the bootstrapper in a subshell,  
you can start using `f` commands in new shells immediately.

Or,  
if you're the cautious type,  
you can clone, build, and install it after verifying the hash (see [doc/readme.md]).


# persistence

The shim,  as installed by the quickstart command, allows the use of aliases (see `core/aliases.sh`) and most `fsh/tool` scripts, but persistence needs root permissions to install the build service.

Running `loop.sh` will, if the `laminar` service 


# scripting
Include `f.sh` in your scripts with:  
`shopt -s expand_aliases; . path/to/f.sh`  
or execute piecemeal as described in [#quickstart](#quickstart)

# legal
All rights reserved 1990-2022,  
all contributions assumed original & freely given,  
non-commercial/hobbiest/educational use encouraged.

## knowledge disclaimer
All of this came from a book or a website somewhere,  
if you believe you work has been referenced,  
thank you for teaching me that particular trick.

For purposes of branding & legality,  
`f.sh`/`fsh`/`f` &copy; 1990-present [@jakimfett](https://jakimfett.com/).

Due to the resurgence of public toleration for neo-nazi identities,  
authoritarian/nationalist/colonial use explicitly prohibited,  
go kick rocks in flip flops, ya gorram fascists.

# commercial support
Contact '[via email](mailto:office@assorted.tech)' or  '[web form](https://assorted.tech/contact)' for commercial licensing & professional use.



# usage
Include functionality in your own scripts with:
```
#!/
DIR=path/to/fsh/
[[ -f $DIR/$1  ]] || { echo "fsh not found"; exit 1; }
[[ "$1" =~ ../ ]] && { echo "Forbidden path"; exit 2; }
source /usr/local/etc/library.sh
cd $DIR
launch_script $1
```
