#!/bin/bash
# returns a precise & human readable date format
echo
date +"%Y_%m_%d%t%t%t%H.%M.%S (%Z/GMT%z)%n%A, %d %B%t%tDay %j"
