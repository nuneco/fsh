For a decent experience with this software,  
you need a base level understanding of Linux shell scripting & [git revision control](https://eliotberriot.com//blog/2019/03/05/building-sotfware-together-with-git/),  
and most likely a Debian-flavoured system to run things on top of.


# Installation

Pick where to put the code.
`mkdir -p ~/src/fsh ~/bin`

Get the code.
`cd ~/src/fsh && git init && git remote add origin https://functions.sh/ && git fetch origin && git merge origin/dev && git checkout dev`

# Interaction

Defaults are non-destructive, see `f --help persist` for writing to disk.

### Examples:
_get dependencies of an installable_  
`f --depends <software(:sym.ver.sion)/>`  

_check installation prerequisites/dependencies for nginx_  
`f --install nginx`
