there has to be a way to learn this with the resources at hand.

# First Hours
I've learned bits and pieces of the Mikrotik RouterOS system,  
and rather a lot of it has 'Nix roots,  
so there's groundwork laid.

Several existing partial attempts at an "autoconfig" script exist,  
they just need combined, condensed, and rendered efficient.

First priority is a window outside.  
Apparently, that's already set up.  
Creating a settings export for posterity.

## Minutes Later:
Soft-bricked again.

Trying to bridge the connection between LAN ports, this time.

No idea how I've kept my shell connection with the microserver.

Taking some time to see if the login problems over there can be solved,  
before tackling the MikroTik problem again.

...am I just delaying?
we'll never truly know...

## MikroTik: Exports
The `export` command from the RouterOS command line produces a line-by-line configuration commands printout. In theory, the system could be wiped, and then the delta between "default" settings and the local backup could be applied, but questions of "what to wipe" and "what to overwrite" and whether having multiple valid configurations will break things has so-far prevented attempts to actually test the backups.

There was documentation of the necessary system-reset-level (via a pinhole button wait time mechanism) for getting a "clean slate", but it's rather easy to softbrick this system, and so far getting it to work seems half luck, half chance, half repeating the gui options until they "stick" in memory.

Have heard, via comments in forums etc, that the command line experience is much...less broken.

Time will tell.
