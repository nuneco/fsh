Once upon a time,  
in BSD v4.0,  
the dmr `#!` became [part of the paradigm](https://www.in-ulm.de/~mascheck/various/shebang/).

 See [beginnings](https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md#how-to-begin-a-bash-script) for errata.
