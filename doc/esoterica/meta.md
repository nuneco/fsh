The meta is simple.

# Facilitate Thy User(base)
Understanding and wisdom are oft separated by time and/or space.

Accessibility in isolation must drive basic considerations.

To this end, many convoluted attempts at beginning, middle, and end may be attempted,
yet never truly finished with the final polish.

## Tenant One
Installation must be simple.

Download image, burn image, run.

More than three steps, and massive falloff in accessibility is evident.

Your userbase needs simple.

Make it so.

`install.sh`

## Tenant Two
Maintenance must be simple.

A thousand levers to accomplish one job is less efficient than a single manual task.

Automate all non-critical decisions, and allow decision-level variations beyond the boolean.

`upgrade.sh`

## Tenant Zero
Removal must be simple.

Preserve your user's data beyond program end.

`remove.sh`
