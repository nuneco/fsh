"Without the structure, this was a bona-fide grey goo scenario."
~ nune

This is a scripting language, for execution on systems that roughly conform with the [Filesystem Heirarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).

# users
Several users are expected to exist.

The `root` user is sorta requirement zero.
Multi-user environment is not required, but beneficial.
Single-user mode is untested, and should be considered pre-alpha.

The `bridge` account is for local network repository sync and facilitating secure network access.

The `build` account processes the local and shared build queue, in that order.

The `host` account serves content for browsers.

# interaction
Methods of input are optimized for cursor jumping, tab completion, and structural abstraction.

A workflow of two keys, tab complete, two keys, tab complete, normalizes the mental macros for interfacing with the system via a QWERTY keyboard.

Define full options, first character is the short tag.  
Thus, `--version` and `-v` are the same command.

Minimalism over features.  
Be mindful of command paradigm [complexity](https://danluu.com/cli-complexity/) and efficiency's simplicity.


# files

The quickstart creates symbolic link from `~/bin/f` to `~/run/fsh/f.sh`, and adds `source ~/.aliases` to your `.bash_aliases` file.

A persistence installation compiles prerequisites and applications, creates users, installs ssh keys, adds a system service, and schedules regular updates.d

```
# structure
~/bin/	# <-- any place on the PATH will do.
	./f	# <-- symlink to ~/run/fsh/f.sh

~/run/	# <-- application run directory, for logfiles & config, data goes elsewhere?
	./fsh/	# <-- root project folder, can be anywhere writeable by the user.
		./cfg/		# <-- locale, path, and structure files.
		./core/		# <-- files essential to the proper functioning of f.sh
			./aliases.src	# <-- quality of life commands, shim installer.
			./loop.sh	# <-- main loop, start debugging here.
		./doc/		# <-- where the wordsplainy bits live.
		./tmp/		# <-- volatile storage, probably frequently wiped.
		./tool/		# <-- freestanding scripts & example usage.

		./f.sh		# <-- bootstrap, installer, core executer.
		./readme.md	# <-- essential reading, new user entry point.

~/src/	# <-- local origin for source code
	./fsh/	# <-- mirror of functions.sh?

# modifies
~/.bash_aliases 	# <-- 'f --install' adds a line 
			(...sourcing 'fsh/core/aliases.src', aka the shim.)
			
# persistence

/usr/sbin/laminard becomes a symlink to /home/build/bin/laminard
/etc/laminar.conf becomes a symlink to /home/build/run/laminar.conf
/etc/
```

# efficiency methodology
Autocomplete for ease of user autocomplete.

Most tools could be made available by using up to twoe characters of hinting along with the tab key. Examples being the refactor of the `conf` directory to be `cfg`, allowing three-key sequences for `co<TAB/>` and `cf<TAB/>`, rather than `cor<TAB/>` and `con<TAB/>`.
