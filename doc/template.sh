#!/bin/bash -u
#

# Pre-emptive variable declaration
declare -A envConf command param

# verbosity, min 0, max 9.
envConf['verbosity']=3

## include f.sh ##
shopt -s expand_aliases;source "path/to/f.sh";
## end includes ##

command['template']="command & usage info."
envConf['description']="Template program, boilerplate mostly."
